﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class ThePathfinding : MonoBehaviour {

    public Transform seeker, target;

    TheGrid grid;

    void Awake()
    {
        grid = GetComponent<TheGrid>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            FindPath(seeker.position, target.position);
        }
    }

    void FindPath(Vector3 startPos, Vector3 targetPos)
    {
        Stopwatch sw = new Stopwatch();
        sw.Start();

        Node_AStar startNode = grid.NodeFromWorldPoint(startPos);
        Node_AStar targetNode = grid.NodeFromWorldPoint(targetPos);

        //Inicia algortimo
        //List<Node_AStar> openSet = new List<Node_AStar>(); //Antes del Heap
        Heap<Node_AStar> openSet = new Heap<Node_AStar>(grid.MaxSize);
        HashSet<Node_AStar> closedSet = new HashSet<Node_AStar>();
        openSet.Add(startNode);

        while (openSet.Count > 0)
        {
            Node_AStar currentNode = openSet.RemoveFirst();
            closedSet.Add(currentNode);

            if(currentNode == targetNode) //Se ha encontrado el camino
            {
                sw.Stop();
                print("Path found: " + sw.ElapsedMilliseconds + " ms");
                RetracePath(startNode, targetNode);
                return;
            }

            foreach (Node_AStar neighbour in grid.GetNeighbours(currentNode))
            {
                if(!neighbour.walkable || closedSet.Contains(neighbour))
                {
                    continue;
                }
                int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
                if(newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                {
                    neighbour.gCost = newMovementCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, targetNode);
                    neighbour.parent = currentNode;

                    if(!openSet.Contains(neighbour))
                    {
                        openSet.Add(neighbour);
                    }
                    else
                    {
                        openSet.UpdateItem(neighbour);
                    }
                }
            }
        }
    }

    void RetracePath(Node_AStar startNode, Node_AStar endNode)
    {
        List<Node_AStar> path = new List<Node_AStar>();
        Node_AStar currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        path.Reverse();

        grid.path = path;
    }

    int GetDistance (Node_AStar nodeA, Node_AStar nodeB)
    {
        int distX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int distY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if(distX > distY)
            return 14 * distY + 10 * (distX - distY);
        else
            return 14 * distX + 10 * (distY - distX);
    }
	
}
