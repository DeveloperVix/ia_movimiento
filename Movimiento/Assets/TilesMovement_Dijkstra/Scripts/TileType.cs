﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TileType {

    public string name;
    public GameObject tilePrefab;

    public bool isWalkable = true;
    public float movementCost = 1;  //El costo que tiene el tile cuando la unidad quiere pasar por ella, dependiendo del juego este sistema podria se mas complejo
                                    //ya que se recomienda ajustar el costo dependiendo el tipo de la unidad, por ejemplo una terrestre y una que vuela
}
