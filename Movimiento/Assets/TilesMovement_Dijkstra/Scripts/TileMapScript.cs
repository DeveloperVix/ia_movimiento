﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TileMapScript : MonoBehaviour {

    public GameObject selectedUnit;

    public TileType[] tileTypes;

    int[,] tiles;
    Node[,] graph;


    int mapSizeX = 10;
    int mapSizeY = 10;

    void Start()
    {
        //setup the selected unit's variable
        selectedUnit.GetComponent<Unit>().tileX = (int)selectedUnit.transform.position.x;
        selectedUnit.GetComponent<Unit>().tileY = (int)selectedUnit.transform.position.y;
        selectedUnit.GetComponent<Unit>().map = this;

        GeneratMapData();
        GeneratePathFindingGraph();
        GenerateMapVisual();
    }

    void GeneratMapData()
    {
        //Allocate our map tiles
        tiles = new int[mapSizeX, mapSizeY];

        int x,y;

        //Initialize our map tiles to be grass
        for (x = 0; x < mapSizeX; x++)
        {
            for (y = 0; y < mapSizeY; y++)
            {
                tiles[x, y] = 0;
            }
        }

        //Make a big swamp area
        for (x = 3; x <= 5; x++)
        {
            for (y = 0; y < 4; y++)
            {
                tiles[x, y] = 1;
            }
        }

        //Creating mountains tile
        tiles[4, 4] = 2;
        tiles[4, 5] = 2;
        tiles[4, 6] = 2;

        tiles[5, 4] = 2;
        tiles[6, 4] = 2;
        tiles[7, 4] = 2;

        tiles[8, 4] = 2;
        tiles[8, 5] = 2;
        tiles[8, 6] = 2;
    }

    public float CostToEnterTile(int sourceX, int sourceY, int targetX, int targetY)
    {
        TileType tt = tileTypes[tiles[targetX, targetY]];

        if (UnitCanEnterTile(targetX, targetY) == false)
        {
            return Mathf.Infinity;
        }

        float cost = tt.movementCost;

        if (sourceX != targetX && sourceY != targetY)
        {
            //We are moving diagonally fudge the cost for tie-breaking
            //Purely a cosmetic thing
            cost += 0.001f;
        }

        return cost;
    }

    void GeneratePathFindingGraph()
    {
        //Initialize the array
        graph = new Node[mapSizeX, mapSizeY]; //cada tile es un nodo

        //Initialize a Node for each spot in the array
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                graph[x, y] = new Node();

                graph[x, y].x = x;
                graph[x, y].y = y;
            }
        }

        //Now that all the nodes exist, calculate their neighbours
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {

                //This is the 4 way connection
                /*if (x > 0)
                    graph[x, y].neighbours.Add(graph[x - 1, y]);
                  if (x < mapSizeX-1)
                    graph[x, y].neighbours.Add(graph[x + 1, y]);
                  if (y > 0)
                    graph[x, y].neighbours.Add(graph[x, y - 1]);
                  if (y < mapSizeY - 1)
                    graph[x, y].neighbours.Add(graph[x, y + 1]);
                */

                //This is the 8 way connection version (allows diagonal movement)
                //Try left
                if (x > 0)
                {
                    graph[x, y].neighbours.Add(graph[x - 1, y]);
                    if (y > 0)
                        graph[x, y].neighbours.Add(graph[x-1, y - 1]);
                    if (y < mapSizeY - 1)
                        graph[x, y].neighbours.Add(graph[x-1, y + 1]);
                }
                
                //Try Right
                if (x < mapSizeX - 1)
                {
                    graph[x, y].neighbours.Add(graph[x + 1, y]);
                    if (y > 0)
                        graph[x, y].neighbours.Add(graph[x + 1, y - 1]);
                    if (y < mapSizeY - 1)
                        graph[x, y].neighbours.Add(graph[x + 1, y + 1]);
                }
                
                //Try straight up and down
                if (y > 0)
                    graph[x, y].neighbours.Add(graph[x, y - 1]);
                if (y < mapSizeY - 1)
                    graph[x, y].neighbours.Add(graph[x, y + 1]);
            }
        }
    }

    void GenerateMapVisual()
    {
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                TileType tt = tileTypes[tiles[x, y]];
                GameObject go = (GameObject) Instantiate(tt.tilePrefab, new Vector3(x, y, 0), Quaternion.identity);

                ClickableTile ct = go.GetComponent<ClickableTile>();
                ct.tileX = x;
                ct.tileY = y;
                ct.map = this;

                go.transform.SetParent(this.transform);
            }
        }
    }

    public Vector3 TileCoordToWorldCoord(int x, int y)
    {
        return new Vector3(x, y, 0);
    }

    public bool UnitCanEnterTile(int x, int y)
    {
        //We could test the unit's walk/hover/fly type against various
        //terrain flags here to see if they are allowed to enter the tile
        return tileTypes[tiles[x,y]].isWalkable;
    }

    //El algoritmo tomando en cuenta el bajo costo por desplazarse en las losetas .-.
    public void GeneratePathTo(int x, int y)
    {
        //Clear out our unit's old path
        selectedUnit.GetComponent<Unit>().currentPath = null;

        if (UnitCanEnterTile(x,y) == false)
        {
            //We probably clicked on a mountain or something, so just quit out
            return;
        }

        /***
        //selectedUnit.GetComponent<Unit>().SetCoords(x, y);
        //selectedUnit.transform.position = TileCoordToWorldCoord(x, y); //Cheat :V
        *****/

        Dictionary<Node, float> dist = new Dictionary<Node, float>();   //Distancia desde el origen al siguiente tile
        Dictionary<Node, Node> prev = new Dictionary<Node, Node>();     //Nodo previo

        //Setup the Q -- the list of nodes we haven't checked yet
        List<Node> unvisited = new List<Node>();

        Node source = graph[selectedUnit.GetComponent<Unit>().tileX, selectedUnit.GetComponent<Unit>().tileY]; //El punto de donde viene la unidad

        Node target = graph[x, y];

        dist[source] = 0;       //Se pone en cero
        prev[source] = null;    //Se pone en nulo

        //Initialize everything to have INFINITY distance, sine we don't know any better right now. Also, it's possible that some nodes CAN'T be reached from the source, wich
        // would make INFINITY a reasonable value
        foreach (Node v in graph)
        {
            if(v != source)
            {
                dist[v] = Mathf.Infinity;
                prev[v] = null;
            }
            unvisited.Add(v);
        }

        while(unvisited.Count > 0)
        {
            //u is going to be the unvisited node with the smallest distance
            Node u = null;

            foreach (Node possibleeU in unvisited)
            {
                if(u == null || dist[possibleeU] < dist[u])
                {
                    u = possibleeU;
                }
            }

            if(u == target)
            {
                break;  //exit the while loop
            }

            unvisited.Remove(u);

            foreach  (Node v in u.neighbours)
            {
                //float alt = dist[u] + u.DistanceTo(v);  //Calcula la distancia al siguiente nodo del nodo vecino
                float alt = dist[u] + CostToEnterTile(u.x, u.y, v.x, v.y); //Se añade el costo del tile para la distancia
                if (alt < dist[v])
                {
                    dist[v] = alt;
                    prev[v] = u;
                }
                
            }
        }

        //if we get here, the either we found the shortest route to our target or in no route at all to our target


        if(prev[target] == null)
        {
            //No route between our target and the source
            return;
        }

        List<Node> currentPath = new List<Node>();
        Node curr = target;

        //Step through the prev chanin and add it to our path
        while (curr != null)
        {
            currentPath.Add(curr);
            curr = prev[curr];
        }

        //Right now, currentPath describes  a route from our target to our source

        currentPath.Reverse();

        selectedUnit.GetComponent<Unit>().currentPath = currentPath;
    }
}
