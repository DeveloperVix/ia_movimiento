﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour {

    public float speed;

    Transform target;

    public float distanceTarget;

	// Use this for initialization
	void Start () {
        target = GameObject.Find("Player").GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        distanceTarget = Vector2.Distance(transform.position, target.position);
        if(Vector2.Distance(transform.position, target.position) > 2)
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

	}
}
