﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float speedMovement = 5f;
    Rigidbody2D rbd2D;
    float axisX;
    float axisY;

	// Use this for initialization
	void Start () {
        rbd2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        rbd2D.velocity = new Vector2(axisX * speedMovement, axisY * speedMovement);
    }

    void Update()
    {
        axisX = Input.GetAxis("Horizontal");
        axisY = Input.GetAxis("Vertical");
    }
}
